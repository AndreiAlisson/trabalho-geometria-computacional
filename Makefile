CFLAGS = -Wall -g -o programa
CC = gcc

all: programa

programa: main.c
	$(CC) $(CFLAGS) main.c

clean:
	rm programa