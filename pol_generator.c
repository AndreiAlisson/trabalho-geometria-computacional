/*Programa que gera arquivos com polígonos no formato do trabalho*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define FILE_SIZE 20  //número máximo de polígonos
#define POL_SIZE 30   //número máximo de segmentos de um polígono
#define POINT_SIZE 30 //maior ponto possível de um polígono

int main(int argc, char *argv[]){

	srand(time(NULL));
	int size, r;

	FILE *f = fopen("input.txt", "w");

	size = rand() % FILE_SIZE;

	fprintf(f, "%d\n", size);

	for(int i = 0; i < size; i++){
		//Tamanho mínimo de um polígono: 3
		do
			r = rand() % POL_SIZE;
		while(r <= 2);
		fprintf(f, "%d\n", r);
		for(int j = 0; j < r; j++){
			fprintf(f, "%d %d\n", rand() % POINT_SIZE, rand() % POINT_SIZE);
		}
	}

	printf("Número de polígonos do arquivo gerado: %d\n", size);

	fclose(f);
	return 0;
}