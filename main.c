//Autores: Andrei Alisson Ferreira Julio GRR20163061

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define POL_SIZE 100 //Tamanho inicial da aloca��o da estrutura Poligono

//Estrutura de dados para o pol�gono
typedef struct Poligono{
    int *x, *y;  //Coordenadas do pol�gono
    int tam;     //Tamanho do pol�gono
    int intersec;//Vari�vel de controle para verifica��o de intersec��o
    int eh_Convexo;
}P;

//Estrutura de dados para guardar o tamanho do fecho convexo
typedef struct QuickH{
	int tam;
}QH;

QH qh;

//Verifica se dois segmentos se intersectam; 0 n�o, 1 sim.
int temIntersec(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4){

    double det, s, t;

    det = (x4 - x3) * (y2 - y1)  -  (y4 - y3) * (x2 - x1);

    //0 -> n�o h� intersec��o
    if(det == 0.0)
        return 0;

    s = ((x4 - x3) * (y3 - y1) - (y4 - y3) * (x3 - x1))/ det;
    t = ((x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1))/ det;

    if(s <= 1.0 && s >= 0.0 && t <= 1.0 && t >= 0.0)
        return 1;
    else
        return 0;
}

//Retorna o lado em que x e y est�o em rela��o � reta
int findSide(int x1, int y1, int x2, int y2, int x, int y){ 

    int val = (y - y1) * (x2 - x1) - 
              (y2 - y1) * (x - x1); 
  
    if (val > 0) 
        return 1; 
    if (val < 0) 
        return -1; 
    return 0; 
} 
  
//Retorna o valor proporcional � dist�ncia entre o ponto x,y e a linha
int lineDist(int x1, int y1, int x2, int y2, int x, int y){ 

    return abs ((y - y1) * (x2 - x1) - 
               (y2 - y1) * (x - x1)); 
} 
  
//Linha composta por (x1,y2),(x2,y2); side � o lado em que um ponto est�, sendo este 1 ou -1
void quickHull(int *x, int *y, int n, int x1, int y1, int x2, int y2, int side){ 

    int ind = -1; 
    int max_dist = 0; 
    int find_side, zero = 0;
  
    //Encontra o ponto com maior dist�ncia da reta
    for(int i = 0; i < n; i++){ 
    	if((x1 != x[i] || y1 != y[i]) && (x2 != x[i] || y2 != y[i])){
	        int temp = lineDist(x1, y1, x2, y2, x[i], y[i]);
	        find_side = findSide(x1, y1, x2, y2, x[i], y[i]);
	        //printf("dist (%d,%d) - (%d,%d) e (%d,%d) = %d\n", x1, y1, x2, y2, x[i], y[i], find_side);
	        if(find_side == side && temp > max_dist){ 
	            ind = i; 
	            max_dist = temp; 
	        }
	        else if(find_side == 0)
	        	zero = 1;
	    }
    } 
  
    //Se nenhum ponto foi encontrado, adiciona os extremos na estrutura e retorna
    //pois acabou a recursividade
    if (ind == -1){ 
        //Se existe um pouco em cima dessa linha ap�s o fim da recurs�o, este deve ser contabilizado como v�lido.
        if(zero == 1)
    		qh.tam++;
        qh.tam++;
        return; 
    }

  
    //Chama recursivamente as duas partes divididas 
    quickHull(x, y, n, x[ind], y[ind], x1, y1, -findSide(x[ind], y[ind], x1, y1, x2, y2)); 
    quickHull(x, y, n, x[ind], y[ind], x2, y2, -findSide(x[ind], y[ind], x2, y2, x1, y1)); 
}

void printHull(int *x, int *y, int n){

	if(n < 3)
		return;

	int min_x = 0, max_x = 0; 
	//Encontra os pontos extremos em x
    for(int i = 0; i < n; i++){ 
        if (x[i] < x[min_x]) 
            min_x = i; 
        if (x[i] > x[max_x]) 
            max_x = i; 
    } 
  
    //Chama procedimento recursivamente de um lado dos extremos
    quickHull(x, y, n, x[min_x], y[min_x], x[max_x], y[max_x], 1); 
  
	//Chama procedimento recursivamente do outro lado dos extremos
    quickHull(x, y, n, x[min_x], y[min_x], x[max_x], y[max_x], -1);

}

int main(int argc, char *argv[])
{
    int m, x, y, t;
    int linhas = 0;
    int i, j, k, l;
    int n = 1;
    int intersect = 0;
    P *p = malloc(POL_SIZE*sizeof(P));

    scanf("%d", &t);

    for(int g = 0; g < t; g++){
        //Leitura do pol�gono
        scanf("%d", &m);

    	if(linhas >= POL_SIZE){
    		n++;
        	p = realloc(p, n*POL_SIZE*sizeof(P));
    	}

        p[linhas].x = malloc(m*sizeof(int));
        p[linhas].y = malloc(m*sizeof(int));
        p[linhas].tam = m;
        qh.tam = 0;

        for(i = 0; i < m; i++){
            scanf("%d %d", &x, &y);
            p[linhas].x[i] = x;
            p[linhas].y[i] = y;
        }

        //Verifica se o pol�gono � simples ou n�o simples
        for(i = 0; i < (m-2); i++){
            for(j = i+(2); j < (m-1); j++)
            	if(temIntersec(p[linhas].x[i], p[linhas].y[i], p[linhas].x[i+1], p[linhas].y[i+1],
          					   p[linhas].x[j], p[linhas].y[j], p[linhas].x[j+1], p[linhas].y[j+1])){
                    p[linhas].intersec = 1;
                    i = j = 2*m; //break
                }
            //Verifica p/ (xmx1),(ymy1), caso n�o seja o segmento inicial  
            if(i > 0) 
            	if(temIntersec(p[linhas].x[i], p[linhas].y[i], p[linhas].x[i+1], p[linhas].y[i+1],
     			               p[linhas].x[j], p[linhas].y[j], p[linhas].x[0], p[linhas].y[0])){
                	p[linhas].intersec = 1;
                	i = j = m; //break
            	}
        }

        if(p[linhas].intersec)
            printf("%d nao simples\n", linhas+1);
        else{
        	printf("%d simples ", linhas+1);           	
        	//Verifica se � convexo
        	printHull(p[linhas].x, p[linhas].y, p[linhas].tam);
        	if(p[linhas].tam == qh.tam){
        		printf("e convexo\n");
        		p[linhas].eh_Convexo = 1;
        	}
        	else{
        		printf("e nao convexo\n");
        		p[linhas].eh_Convexo = 0;
        	}
        }
        linhas++;

    }

    //Varre todos os segmentos entre os pol�gonos para encontrar intersec��o
    for(i = 0; i < linhas - 1; i++)
    	if(p[i].eh_Convexo){
	    	for(j = i + 1; j < linhas; j++){
	    		if(p[j].eh_Convexo){
		    		for(k = 0; k < p[i].tam - 1 && !intersect; k++){
		    			for(l = 0; l < p[j].tam - 1 && !intersect; l++){
		    				if(temIntersec(p[i].x[k], p[i].y[k], p[i].x[k+1], p[i].y[k+1],
		    							   p[j].x[l], p[j].y[l], p[j].x[l+1], p[j].y[l+1])){
		    					printf("(%d,%d)\n", i+1, j+1);
		    					k = p[i].tam;  //break
		    					l = p[j].tam;  //break
		    					intersect = 1;
		    				}
		    			}

		    			if(!intersect)
					    	if(temIntersec(p[i].x[k], p[i].y[k], p[i].x[k+1], p[i].y[k+1],
			    						   p[j].x[l], p[j].y[l], p[j].x[0], p[j].y[0])){
								printf("(%d,%d)\n", i+1, j+1);
								k = p[i].tam;  //break
								l = p[j].tam;  //break
								intersect = 1;
			    			}
		    		}
		    		if(!intersect){
			    		for(l = 0; l < p[l].tam - 1 &&!intersect; l++)
							if(temIntersec(p[i].x[k], p[i].y[k], p[i].x[0], p[i].y[0],
										   p[j].x[l], p[j].y[l], p[j].x[l+1], p[j].y[l+1])){
								printf("(%d,%d)\n", i+1, j+1);
								k = p[i].tam;  //break
								l = p[j].tam;  //break
								intersect = 1;
							}
						if(!intersect){
					    	if(temIntersec(p[i].x[k], p[i].y[k], p[i].x[0], p[i].y[0],
										   p[j].x[l], p[j].y[l], p[j].x[0], p[j].y[0])){
								printf("(%d,%d)\n", i+1, j+1);
								k = p[i].tam;  //break
								l = p[j].tam;  //break
								intersect = 1;
							}
						}
					}
				}
				intersect = 0;
	    	}
	    }
    				
    //Desaloca mem�ria das estruturas utilizadas
    for(i = 0; i < linhas; i++){
        free(p[i].x);
        free(p[i].y);
    }
     free(p);

    return 0;
}
